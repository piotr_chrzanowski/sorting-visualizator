FROM ubuntu:20.04

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive
ARG QT_DOWNLOAD_DIR="QT6.2.2"
ARG QT_DOWNLOAD_PATH="/QT6.2.2/qt-everywhere-src-6.2.2/"
ARG QT_INSTALLATION_PATH="/opt/Qt6.2.2"


RUN apt-get update && apt-get upgrade -y 
RUN apt-get install -y build-essential
RUN apt-get install -y vim
RUN apt-get install -y wget
RUN apt-get install -y cmake
RUN apt-get install -y git
RUN apt-get install tar
RUN apt-get -y install valgrind
RUN apt-get update && apt-get install -y libfontconfig1-dev \
 libfreetype6-dev libx11-dev libx11-xcb-dev \
 libxext-dev \
 libxfixes-dev \
 libxi-dev \
 libxrender-dev \
 libxcb1-dev \
 libxcb-glx0-dev \
 libxcb-keysyms1-dev \
 libxcb-image0-dev \
 libxcb-shm0-dev \
 libxcb-icccm4-dev \
 libxcb-sync-dev \
 libxcb-xfixes0-dev \
 libxcb-shape0-dev \
 libxcb-randr0-dev \
 libxcb-render-util0-dev \
 libxcb-util-dev \
 libxcb-xinerama0-dev \
 libxcb-xkb-dev \
 libxkbcommon-dev \
 libxkbcommon-x11-dev \
 x11vnc xvfb
 
RUN apt-get update && apt-get install -y python-dev python3-dev

RUN mkdir $QT_DOWNLOAD_DIR && \
cd  $QT_DOWNLOAD_DIR && \
wget https://download.qt.io/official_releases/qt/6.2/6.2.2/single/qt-everywhere-src-6.2.2.tar.xz && \
tar -xf qt-everywhere-src-6.2.2.tar.xz

RUN cd $QT_DOWNLOAD_PATH && \
./configure -prefix $QT_INSTALLATION_PATH \
-skip qtwebsockets \
-skip qtwebview  \
-skip qtwebengine \
-skip qtwebchannel \
-skip qtsvg \
-skip qtwayland \
-skip qtlottie \
-skip qtvirtualkeyboard \
-skip qttranslations \
-skip qt3d \
-skip qtquick3d \
-skip qtopcua \
-skip qtremoteobjects \
-skip qtscxml \
-skip qtcoap \
-skip cmake \
-skip qtconnectivity \
-skip qtsensors \
-skip qtimageformats \
-skip qtactiveqt \
-skip qtmultimedia \
-skip qtmqtt \
-skip coin \
-skip qtnetworkauth \
-skip qtserialport \
-skip qtserialbus \
-skip qtpositioning \
-skip qtdoc \
-skip qt5compat \
-nomake tools -nomake examples -no-opengl 

WORKDIR $QT_DOWNLOAD_PATH

RUN make -j4 && make install

RUN wget https://github.com/QuasarApp/CQtDeployer/releases/download/1.5.3/CQtDeployer_1.5.3.0_Linux64.deb
RUN dpkg -i CQtDeployer_1.5.3.0_Linux64.deb

WORKDIR "/"

