import QtQuick
import QtQuick.Window
import QtQuick.Controls 2.1
import QtCharts


Window {
    visible: true
    width: 800
    height: 600

    Background
    {
        SortingChart
        {
            id : sortingChart
            anchors.margins : 10
            anchors.top: parent.top
            anchors.left: parent.left
        }

        SimulationButtons
        {
            id : simulationButtons
            anchors.margins : 10
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: sortingChart.horizontalCenter
        }

        StatisticsPanel
        {
            id : statisticsPanel
            anchors.margins : 10
            anchors.top: parent.top
            anchors.right: parent.right
        }

        SliderPanel
        {
            id : sliderPanel
            anchors.margins : 10
            anchors.top: statisticsPanel.bottom
            anchors.right: parent.right
        }

        AlgorithmPanel
        {
            id : algorithmPanel
            anchors.leftMargin : 10
            anchors.rightMargin : 10
            anchors.top: sliderPanel.bottom
            anchors.right: parent.right
        }

    }

}
