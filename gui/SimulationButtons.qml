import QtQuick 2.0
import QtQuick.Controls 2.1

Rectangle
{
    id: simulationButtons
    width: (parent.width * 4/5) - 10
    height: (parent.height * 1/5) - 10
    color: parent.color
    border.color: "white"

    Row {
        id: buttonsRow
        spacing: simulationButtons.width/40
        anchors.margins: spacing
        anchors.centerIn: parent

        Button {

            id: startButton
            width: simulationButtons.width / 6
            height: simulationButtons.height / 3
            text: qsTr("Start")
            font.bold: true
            enabled: !simulationController.isStartLocked


            onClicked: {
                simulationButtons.startButtonClicked();            
            }
        }

        Button {
            id: stopButton
            width: simulationButtons.width / 6
            height: simulationButtons.height / 3
            text: qsTr("Stop")
            font.bold: true
            enabled: !simulationController.isStopLocked

            onClicked: {
               simulationButtons.stopButtonClicked()
            }
        }

        Button {
            id: nextButton
            width: simulationButtons.width / 6
            height: simulationButtons.height / 3
            text: qsTr("Next")
            font.bold: true
            enabled: !simulationController.isNextLocked


            onClicked: {
                simulationButtons.nextButtonClicked()
            }
        }

        Button {
            id: previousButton
            width: simulationButtons.width / 6
            height: simulationButtons.height / 3
            text: qsTr("Previous")
            font.bold: true
            enabled: !simulationController.isPreviousLocked

            
            onClicked: {
                simulationButtons.previousButtonClicked()

            }
        }

        Button {
            id: resetButton
            width: simulationButtons.width / 6
            height: simulationButtons.height / 3
            text: qsTr("Reset")
            font.bold: true
            enabled: !simulationController.isResetButtonLocked

            
            onClicked: {
                simulationButtons.resetButtonClicked()

            }
        }
    }

    function startButtonClicked() {
        simulationController.start();
    }

    function stopButtonClicked() {
        simulationController.stop();
    }

    function nextButtonClicked() {
        simulationController.next();
    }

    function previousButtonClicked() {
        simulationController.previous();
    }

    function resetButtonClicked() {
        simulationController.reset();
    }
}

