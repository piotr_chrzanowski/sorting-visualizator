import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle
{
    id: algorithmPanel
    width: (parent.width * 1/5) - 10
    height: (parent.height * 1/3) - 10
    color: parent.color
    border.color : "white"

    ComboBox {
        id: box
        width: algorithmPanel.width - 20
        height: algorithmPanel.height/6
        anchors.topMargin: algorithmPanel.height/6
        anchors.top: parent.top
        anchors.horizontalCenter: algorithmPanel.horizontalCenter
        font.bold: true
        enabled: !simulationController.isAlgorithmBoxLocked
        currentIndex: 0

        textRole: "text"

        model: ListModel {
            id: model
            ListElement { text: "Selection Sort"}
            ListElement { text: "Bubble Sort" }
            ListElement { text: "Quick Sort" }
            // ListElement { text: "Merge Sort" }

        }

        popup: Popup {
            y: box.height - 1
            width: box.width
            padding: 1

            contentItem: ListView {
                id: list
                implicitHeight: contentHeight
                model: box.popup.visible ? box.delegateModel : null
                currentIndex: box.highlightedIndex
                anchors.centerIn: parent
                ScrollIndicator.vertical: ScrollIndicator { }
            }

            background: Rectangle {
                border.color: "gray"
                color: "white"
                radius: 2
            }

        }

        onActivated:
        {
            algorithmPanel.sortingAlgorithmChanged(currentIndex)
            console.log(box.textAt(currentIndex))
        }
    }

    function sortingAlgorithmChanged(value: int) {
        simulationController.setSortingAlgorithm(value);
    }


}
