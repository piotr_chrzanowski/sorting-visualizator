import QtQuick 2.0
import QtCharts 2.1
import QtQuick.Controls 2.1


Rectangle
{
    id: sortingChart
    width: (parent.width * 4/5) - 10
    height: (parent.height * 4/5) - 10
    border.color: "white"
    color: parent.color

    ChartView {
        id: chartView
        width: parent.width
        height: parent.height
        title:  "<b>Sorting Visualization</b>"
        anchors.fill: parent
        legend.visible: false
        antialiasing: true

        ValuesAxis {
            id: yAxis
            min: 0
            max: 100
        }

        BarSeries {
            id: barSeries
            axisY: yAxis
        }

        Component.onCompleted:
        {
            chartController.connect(barSeries)
            simulationController.init()
        }
    }
}

