import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle
{
    id: sliders
    width: (parent.width *1/5) - 10
    height: (parent.height *1/3) - 10
    color: parent.color
    border.color: "white"

    Column
    {
        spacing: parent.height/6
        anchors.centerIn: parent
    
        Column
        {
            spacing: 0

            Rectangle
            {
                width:pointsSlider.width
                height:pointsSlider.height/2
                color: "white" 

                Text {
                    id: pointsText
                    anchors.centerIn: parent
                    text: qsTr("Points: ") + pointsSlider.value
                    font.bold: true
                    color : pointsSlider.enabled ? "black" : "gray"
                }
            }

            Slider {
                id: pointsSlider
                width: sliders.width - 20
                height: sliders.height / 6
                value: 10
                stepSize: 5
                focus: true
                layer.enabled: true
                to: 50
                from: 10
                enabled: !simulationController.isPointsSliderLocked

                onValueChanged:
                {
                    sliders.pointsSliderValueChanged(pointsSlider.value)
                }
            }
        }

        Column
        {
            spacing: 0

            Rectangle
            {
                width: speedSlider.width
                height: speedSlider.height/2
                color: "white"

                Text {
                    id: speedSliderText
                    anchors.centerIn: parent
                    text: qsTr("Speed: ") + speedSlider.value + qsTr("%")
                    font.bold: true
                }
            }

            Slider 
            {
                id: speedSlider
                width: sliders.width - 20
                height: sliders.height / 6
                value: 10
                stepSize: 10
                to: 100
                from: 10

                onValueChanged:
                {
                    sliders.speedSliderValueChanged(speedSlider.value)
                }
            }
        }
    }

    function pointsSliderValueChanged(value: int) {
        simulationController.setSimulationPointsNumber(value);
    }

    function speedSliderValueChanged(value: int) {
        simulationController.setSimulationTimeInterval(value);
    }

}

