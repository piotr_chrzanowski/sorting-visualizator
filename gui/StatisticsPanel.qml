import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle
{
    id: statistics
    width: (parent.width * 1/5) - 10
    height: (parent.height * 1/3) - 10
    color: parent.color
    border.color : "white"

    Column
    {
        spacing : parent.height/10
        anchors.centerIn: parent

        Rectangle
        {
            id : comparisonRectange
            width: statisticsPanel.width - 20
            height: statisticsPanel.height/4
            color: "white" 

            Column
            {
                spacing : parent.height/10
                anchors.centerIn: parent
           
                Text {
                    id: comparisonText
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Comparison count")
                    font.bold: true
                    color : "green"
                }

                Text {
                    id: comparisonCount
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: statisticsPanelController.getComparisonCount
                    font.bold: true
                    color : "green"
                }
            }
        }

        Rectangle
        {
            id : swapRectange
            width: statisticsPanel.width - 20
            height: statisticsPanel.height/4
            color: "white" 

            Column
            {
                spacing : parent.height/10
                anchors.centerIn: parent
           
                Text {
                    id: swapText
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Swap count") 
                    font.bold: true
                    color : "red"
                }

                Text {
                    id: swapCount
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: statisticsPanelController.getSwapCount
                    font.bold: true
                    color : "red"
                }
            }
        }
    }
}