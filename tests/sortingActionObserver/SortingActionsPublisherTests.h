#pragma once
#include "Notifications.h"
#include "SortingActionsPublisher.h"
#include "SortingActionsConverter.h"
#include "publisherMock.h"
#include "subscriberMock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace ::testing;

class PublisherFixture : public testing::Test
{
public:
    void SetUp()
    {
        mockSubscriberFirst = std::make_shared<NiceMock<SubscriberMock>>();
        mockSubscriberSecond = std::make_shared<NiceMock<SubscriberMock>>();
        publisher = std::make_shared<SortingActionsPublisher>();
    }

public:
    std::shared_ptr<NiceMock<SubscriberMock>> mockSubscriberFirst;
    std::shared_ptr<NiceMock<SubscriberMock>> mockSubscriberSecond;

    std::shared_ptr<IPublisher> publisher;
};
