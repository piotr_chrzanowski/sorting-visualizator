#include "SortingActionsPublisherTests.h"

TEST_F(PublisherFixture, Publisher_Should_Add_Subscribers_To_Vector_Via_Subscribe_Method)
{
    EXPECT_TRUE(publisher->subscribe(mockSubscriberFirst));
    EXPECT_FALSE(publisher->subscribe(mockSubscriberFirst));
}

TEST_F(PublisherFixture, Publisher_Should_Remove_Subscribers_To_Vector_Via_Unsubscribe_Method)
{
    EXPECT_TRUE(publisher->subscribe(mockSubscriberFirst));
    EXPECT_TRUE(publisher->unsubscribe(mockSubscriberFirst));
    EXPECT_FALSE(publisher->unsubscribe(mockSubscriberFirst));
}

TEST_F(PublisherFixture, Publisher_Should_Notify_All_Subscribers_Via_Notify_Method)
{
    EXPECT_CALL(*mockSubscriberFirst, update(testing::_)).Times(1);
    EXPECT_CALL(*mockSubscriberSecond, update(testing::_)).Times(1);

    publisher->subscribe(mockSubscriberFirst);
    publisher->subscribe(mockSubscriberSecond);

    publisher->notify(Notification{.type = NotificationType::Comparison, .idxFirst = 0, .idxSecond = 0});
}

TEST_F(PublisherFixture, Publisher_Should_Not_Notify_Subscriber_Who_Was_Previously_Removed)
{
    EXPECT_CALL(*mockSubscriberFirst, update(testing::_)).Times(0);
    EXPECT_CALL(*mockSubscriberSecond, update(testing::_)).Times(1);

    publisher->subscribe(mockSubscriberFirst);
    publisher->subscribe(mockSubscriberSecond);
    publisher->unsubscribe(mockSubscriberFirst);

    publisher->notify(Notification{.type = NotificationType::Comparison, .idxFirst = 0, .idxSecond = 0});

}


