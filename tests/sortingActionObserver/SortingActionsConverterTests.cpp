#include "SortingActionsConverterTests.h"
#include <iostream>

TEST_F(ActionsConverterTestFixture, Subscriber_Init_Method_Should_Register_Itself_In_Publishers_List)
{
    EXPECT_CALL(*mockPublisher, subscribe(testing::_));

    actionConverter->init();
}

TEST_F(ActionsConverterTestFixture, Subscriber_Init_Method_Should_NOT_Register_Itself_In_Publishers_List_When_Publisher_Not_Provided)
{
    EXPECT_CALL(*mockPublisher, subscribe(testing::_)).Times(0);

    actionConverter = std::make_shared<SortingActionsConverter>(nullptr, nullptr, nullptr);

    actionConverter->init();
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Register_Actions)
{
    size_t actionCount = 3;
    feedSubscriberWithNotifications(actionCount);

    EXPECT_EQ(actionConverter->getActionsCount(), actionCount);
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Clear_Actions)
{
    size_t actionCount = 3;
    feedSubscriberWithNotifications(actionCount);
    EXPECT_EQ(actionConverter->getActionsCount(), actionCount);

    actionConverter->clearActions();

    EXPECT_EQ(actionConverter->getActionsCount(), 0);
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_NOT_Proceed_Next_Action_When_Action_List_Empty)
{
    EXPECT_FALSE(actionConverter->nextAction());
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Clear_Selected_Bars_Before_New_Action)
{
    EXPECT_CALL(*mockController, deselectAllBars()).Times(1);

    triggerFakeComparisonAction();

    EXPECT_TRUE(actionConverter->nextAction());
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Color_Bars_Green_When_Comparison_Action_Proceeded)
{
    EXPECT_CALL(*mockController, selectBar(indexFirst)).Times(1);
    EXPECT_CALL(*mockController, selectBar(indexSecond)).Times(1);
    EXPECT_CALL(*mockController, colorSelectedBars(QColor(Qt::green))).Times(1);

    triggerFakeComparisonAction();

    EXPECT_TRUE(actionConverter->nextAction());
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Color_Bars_Red_When_Swap_Comparison_Action_Proceeded)
{
    EXPECT_CALL(*mockController, selectBar(indexFirst)).Times(1);
    EXPECT_CALL(*mockController, selectBar(indexSecond)).Times(1);
    EXPECT_CALL(*mockController, colorSelectedBars(QColor(Qt::red))).Times(1);

    triggerFakeSwapComparisonAction();

    EXPECT_TRUE(actionConverter->nextAction());
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Swap_Bars_When_Swap_Action_Proceeded)
{
    EXPECT_CALL(*mockController, swapBars(indexFirst, indexSecond)).Times(1);

    triggerFakeSwapFoundAction();

    EXPECT_TRUE(actionConverter->nextAction());
}

TEST_F(ActionsConverterTestFixture, Subscriber_Should_Not_Take_Previous_Action_When_ActionID_Reach_Zero)
{
    triggerFakeSwapFoundAction();

    EXPECT_FALSE(actionConverter->previousAction());
}

TEST_F(ActionsConverterTestFixture, Subscriber_Generate_Method_Should_Trigger_Bars_Generation)
{
    std::vector<int> points = {1, 2, 3, 4, 5};
    EXPECT_CALL(*mockController, generateBars(points)).Times(1);

    actionConverter->generate(points);
}

TEST_F(ActionsConverterTestFixture, Subscriber_ClearActions_Should_Update_Statistics_Info)
{
    EXPECT_CALL(*mockStatisticsController, updateStatisticsInfo(testing::_)).Times(1);

    actionConverter->clearActions();
}

TEST_F(ActionsConverterTestFixture, Subscriber_ClearActions_Should_Deselect_All_Bars)
{
    EXPECT_CALL(*mockController, deselectAllBars()).Times(1);

    actionConverter->clearActions();
}

