#pragma once
#include "IChartController.h"
#include "Notifications.h"
#include "SortingActionsConverter.h"
#include "SortingActionsPublisher.h"
#include "statisticsPanelControllerMock.h"
#include "chartControllerMock.h"
#include "publisherMock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace ::testing;

class ActionsConverterTestFixture : public testing::Test
{
public:
    static constexpr size_t indexFirst = 0;
    static constexpr size_t indexSecond = 1;

    void feedSubscriberWithNotifications(size_t n);
    void triggerFakeComparisonAction();
    void triggerFakeSwapComparisonAction();
    void triggerFakeSwapFoundAction();
    void triggerFakeMinimumFoundAction();

    void SetUp()
    {
        mockController = std::make_shared<NiceMock<ChartControllerMock>>();
        mockPublisher = std::make_shared<NiceMock<PublisherMock>>();
        mockStatisticsController = std::make_shared<NiceMock<StatisticsPanelControllerMock>>();
        actionConverter = std::make_shared<SortingActionsConverter>(mockPublisher, mockController, mockStatisticsController);
    }

public:
    std::shared_ptr<NiceMock<ChartControllerMock>> mockController;
    std::shared_ptr<NiceMock<PublisherMock>> mockPublisher;
    std::shared_ptr<NiceMock<StatisticsPanelControllerMock>> mockStatisticsController;
    std::shared_ptr<IActionsConverter> actionConverter;
};

void ActionsConverterTestFixture::feedSubscriberWithNotifications(size_t n)
{
    for (size_t i = 0; i < n; ++i)
    {
        actionConverter->update(Notification{.type = NotificationType::Comparison, .idxFirst = indexFirst, .idxSecond = indexSecond});
    }
}

void ActionsConverterTestFixture::triggerFakeComparisonAction()
{
    actionConverter->update(Notification{.type = NotificationType::Comparison, .idxFirst = indexFirst, .idxSecond = indexSecond});
}

void ActionsConverterTestFixture::triggerFakeSwapComparisonAction()
{
    actionConverter->update(Notification{.type = NotificationType::SwapComparison, .idxFirst = indexFirst, .idxSecond = indexSecond});
}

void ActionsConverterTestFixture::triggerFakeSwapFoundAction()
{
    actionConverter->update(Notification{.type = NotificationType::Swap, .idxFirst = indexFirst, .idxSecond = indexSecond});
}
