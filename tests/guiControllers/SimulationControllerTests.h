#pragma once
#include "SimulationController.h"
#include "randomVectorGeneratorMock.h"
#include "sortingControllerMock.h"
#include "actionsConverterMock.h"
#include "timerMock.h"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace ::testing;

class SimulationControllerFixture : public testing::Test
{
public:
    static constexpr size_t newSliderValue = 10;
    std::vector<int> testVector = {1, 2, 3, 4, 5};

public:
    void SetUp()
    {
        rvgMock = std::make_shared<NiceMock<RandomVectorGeneratorMock>>();
        sortingControllerMock = std::make_shared<NiceMock<SortingControllerMock>>();
        sortingActionsConverterMock = std::make_shared<NiceMock<ActionsConverterMock>>();
        timerMock = std::make_shared<NiceMock<TimerMock>>();

        simulationController = std::make_shared<SimulationController>(sortingControllerMock, sortingActionsConverterMock, rvgMock, timerMock);
    }

public:
    std::shared_ptr<NiceMock<RandomVectorGeneratorMock>> rvgMock;
    std::shared_ptr<NiceMock<TimerMock>> timerMock;

    std::shared_ptr<NiceMock<SortingControllerMock>> sortingControllerMock;
    std::shared_ptr<NiceMock<ActionsConverterMock>> sortingActionsConverterMock;

    std::shared_ptr<SimulationController> simulationController;
};
