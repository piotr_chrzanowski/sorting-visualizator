#include "SimulationControllerTests.h"

TEST_F(SimulationControllerFixture, Controller_Init_Method_Should_Generate_Random_Vector_With_Default_Point_Number)
{
    EXPECT_CALL(*rvgMock, generate(SimulationController::barsSize, SimulationController::barLowerBound, SimulationController::barUpperBound, testing::_)).Times(1);

    EXPECT_CALL(*sortingActionsConverterMock, init()).Times(1);
    EXPECT_CALL(*sortingActionsConverterMock, generate(testing::_)).Times(1);

    simulationController->init();
}

TEST_F(SimulationControllerFixture, Controller_Init_Method_Should_Init_SortingActions_Converter)
{
    EXPECT_CALL(*sortingActionsConverterMock, init()).Times(1);

    simulationController->init();
}

TEST_F(SimulationControllerFixture, Controller_Init_Method_Should_Generate_Bars_On_The_Plot)
{
    EXPECT_CALL(*sortingActionsConverterMock, generate(testing::_)).Times(1);

    simulationController->init();
}

TEST_F(SimulationControllerFixture, Controller_Init_Method_Should_Start_Sorting_Action_For_Default_Vector_And_Default_Algorithm)
{
    EXPECT_CALL(*sortingControllerMock, sort(testing::_)).Times(1);

    simulationController->init();
}

TEST_F(SimulationControllerFixture, StartButton_Should_Lock_StartButton)
{
    simulationController->start();

    EXPECT_TRUE(simulationController->isStartLocked());
}

TEST_F(SimulationControllerFixture, StartButton_Should_UnLock_StopButton)
{
    simulationController->start();

    EXPECT_FALSE(simulationController->isStopLocked());
}

TEST_F(SimulationControllerFixture, StartButton_Should_Lock_Points_Sliders)
{
    simulationController->start();

    EXPECT_TRUE(simulationController->isPointsSliderLocked());
}

TEST_F(SimulationControllerFixture, StartButton_Should_Lock_Algorithm_Box)
{
    simulationController->start();

    EXPECT_TRUE(simulationController->isAlgorithmBoxLocked());
}

TEST_F(SimulationControllerFixture, StartButton_Should_Start_Timer)
{
    EXPECT_CALL(*timerMock, start()).Times(1);

    simulationController->start();
}

TEST_F(SimulationControllerFixture, StartButton_Should_Lock_Next_And_Previous_Buttons)
{
    simulationController->start();
    EXPECT_TRUE(simulationController->isNextLocked());
    EXPECT_TRUE(simulationController->isPreviousLocked());
}

TEST_F(SimulationControllerFixture, StartButton_Should_Lock_Reset_Button)
{
    simulationController->start();
    EXPECT_TRUE(simulationController->isResetLocked());
}

TEST_F(SimulationControllerFixture, StopButton_Should_UnLock_Next_And_Previous_Buttons)
{
    simulationController->start();
    simulationController->stop();
    EXPECT_FALSE(simulationController->isNextLocked());
    EXPECT_FALSE(simulationController->isPreviousLocked());
}

TEST_F(SimulationControllerFixture, StopButton_Should_UnLock_Reset_Button)
{
    simulationController->start();
    simulationController->stop();
    EXPECT_FALSE(simulationController->isResetLocked());
}

TEST_F(SimulationControllerFixture, StopButton_Should_Stop_Timer)
{
    EXPECT_CALL(*timerMock, stop()).Times(1);

    simulationController->stop();
}

TEST_F(SimulationControllerFixture, StopButton_Should_Unlock_Points_Silder)
{
    simulationController->start();
    simulationController->stop();

    EXPECT_FALSE(simulationController->isPointsSliderLocked());
}

TEST_F(SimulationControllerFixture, StopButton_Should_Unlock_Algorithm_Box)
{
    simulationController->start();
    simulationController->stop();

    EXPECT_FALSE(simulationController->isAlgorithmBoxLocked());
}

TEST_F(SimulationControllerFixture, StopButton_Should_Unlock_StartButton)
{
    simulationController->start();
    simulationController->stop();

    EXPECT_FALSE(simulationController->isStartLocked());
}

TEST_F(SimulationControllerFixture, StopButton_Should_Lock_StopButton)
{
    simulationController->start();
    simulationController->stop();

    EXPECT_TRUE(simulationController->isStopLocked());
}

TEST_F(SimulationControllerFixture, NextButton_Shoud_Process_Next_Sorting_Action)
{
    EXPECT_CALL(*sortingActionsConverterMock, nextAction()).Times(1);
    simulationController->next();
}

TEST_F(SimulationControllerFixture, NextButton_Shoud_Process_Previous_Sorting_Action)
{
    EXPECT_CALL(*sortingActionsConverterMock, previousAction()).Times(1);
    simulationController->previous();
}

TEST_F(SimulationControllerFixture, ResetButton_Shoud_Reset_Simulation_Chart_To_State_Before_Sorting)
{
    EXPECT_CALL(*sortingActionsConverterMock, clearActions()).Times(1);
    EXPECT_CALL(*sortingControllerMock, sort(testing::_)).Times(1);
    EXPECT_CALL(*sortingActionsConverterMock, generate(testing::_)).Times(1);

    simulationController->reset();
}

TEST_F(SimulationControllerFixture, PointsSlider_Value_Update_Should_Generate_New_Random_Vector)
{
    EXPECT_CALL(*rvgMock, generate(newSliderValue, SimulationController::barLowerBound, SimulationController::barUpperBound, testing::_)).Times(1);
    EXPECT_CALL(*sortingActionsConverterMock, generate(testing::_)).Times(1);

    simulationController->setSimulationPointsNumber(newSliderValue);
}

TEST_F(SimulationControllerFixture, PointsSlider_Value_Update_Should_Clear_Previous_Sorting_Actions)
{
    EXPECT_CALL(*sortingActionsConverterMock, clearActions()).Times(1);

    simulationController->setSimulationPointsNumber(newSliderValue);
}

TEST_F(SimulationControllerFixture, PointsSlider_Value_Update_Should_Sort_New_Generated_Vector)
{
    EXPECT_CALL(*sortingControllerMock, sort(testing::_)).Times(1);

    simulationController->setSimulationPointsNumber(newSliderValue);
}

TEST_F(SimulationControllerFixture, SpeedSlider_Value_Update_Should_Modify_Simulation_Speed)
{
    size_t interval1 = 10;
    size_t interval2 = 50;
    size_t interval3 = 100;

    simulationController->setSimulationTimeInterval(interval1);
    EXPECT_EQ(simulationController->getSimulationTimeInterval(), SimulationController::baseTimeInterval / (interval1 + 1) + SimulationController::minimalTimeInterval);

    simulationController->setSimulationTimeInterval(interval2);
    EXPECT_EQ(simulationController->getSimulationTimeInterval(), SimulationController::baseTimeInterval / (interval2 + 1) + SimulationController::minimalTimeInterval);

    simulationController->setSimulationTimeInterval(interval3);
    EXPECT_EQ(simulationController->getSimulationTimeInterval(), SimulationController::baseTimeInterval / (interval3 + 1) + SimulationController::minimalTimeInterval);
}

TEST_F(SimulationControllerFixture, Algorithm_Panel_Change_Should_Clear_All_Sorting_Actions_And_Sort_To_Get_New_One)
{
    EXPECT_CALL(*sortingActionsConverterMock, clearActions()).Times(1);
    EXPECT_CALL(*sortingControllerMock, sort(testing::_)).Times(1);

    simulationController->setSortingAlgorithm(0);
}

TEST_F(SimulationControllerFixture, Timeout_Function_Should_Trigger_NextAction)
{
    EXPECT_CALL(*sortingActionsConverterMock, nextAction()).Times(1);

    simulationController->timeout();
}
