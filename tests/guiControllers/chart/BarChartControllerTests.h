#pragma once
#include "BarChartController.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace ::testing;

class BarChartControllerFixture : public testing::Test
{
public:


    void SetUp()
    {
        barChartController = std::make_shared<BarChartController>();
    }

public:

    std::shared_ptr<BarChartController> barChartController;
};
