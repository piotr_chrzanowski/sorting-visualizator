#pragma once
#include "Timer.h"
#include <chrono>
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace ::testing;

class TimerTestFixture : public testing::Test
{
public:
    static constexpr size_t testInterval_ms = 100;
    static constexpr size_t timeError_ms = 10;


    void SetUp()
    {
        timer = std::make_shared<TimeoutTimer>();
        start = std::chrono::system_clock::now();
        stop = start;
    }

public:

    std::shared_ptr<ITimer> timer;
    std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> stop;
    std::chrono::duration<float> difference;
};
