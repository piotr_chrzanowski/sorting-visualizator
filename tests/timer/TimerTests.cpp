#include "TimerTests.h"
/*
TEST_F(TimerTestFixture, Starting_Timer_With_Callback_Will_Call_It_After_1_ms_By_Default)
{
    size_t callingCount = 0;

    timer->setTimeoutCallback([&callingCount, this]() {
        callingCount++;
        stop = std::chrono::system_clock::now();
    });

    start = std::chrono::system_clock::now();
    timer->start();
    usleep(1);
    EXPECT_TRUE(timer->isTimerStarted());
    timer->stop();
    difference = stop - start;

    EXPECT_LT(difference.count(), static_cast<float>((TimeoutTimer::defaultInterval_ms + timeError_ms) / 1000.00));

    EXPECT_EQ(callingCount, 1);
}

TEST_F(TimerTestFixture, Changing_Interval_Change_Timer_Interval)
{
    const size_t newInterval = 10;
    timer->setTimeoutCallback([this]() {
        stop = std::chrono::system_clock::now();
    });

    timer->setNewInterval(10);
    start = std::chrono::system_clock::now();
    timer->start();
    usleep(1);

    EXPECT_TRUE(timer->isTimerStarted());
    timer->stop();
    difference = stop - start;

    EXPECT_LT(difference.count(), static_cast<float>((newInterval + timeError_ms) / 1000.00));
}
*/