#include "RandomVectorGeneratorTests.h"

TEST_F(RandomVectorGeneratorFixture, Generate_Method_Should_NOT_Generate_Vector_When_Bound_Are_Invalid)
{
    std::vector<int> testVector;
    randomVectorGenerator->generate(vectorSize, invalidLowerBound, invalidUpperBound, testVector);

    EXPECT_TRUE(testVector.empty());
}

TEST_F(RandomVectorGeneratorFixture, Generate_Method_Should_Generate_Vector_Of_Specific_Size_From_Bounds)
{
    std::vector<int> testVector;
    randomVectorGenerator->generate(vectorSize, lowerBound, upperBound, testVector);

    for(auto &elem : testVector)
    {
        EXPECT_TRUE(elem >= lowerBound && elem <= upperBound);
    }

    EXPECT_EQ(testVector.size(), vectorSize);
}