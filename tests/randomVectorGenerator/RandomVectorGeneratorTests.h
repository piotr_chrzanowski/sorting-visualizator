#pragma once
#include "Notifications.h"
#include "RandomVectorGenerator.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace ::testing;

class RandomVectorGeneratorFixture : public testing::Test
{
public:
    static constexpr size_t vectorSize = 10;
    static constexpr int lowerBound = 0;
    static constexpr int upperBound = 100;

    static constexpr int invalidLowerBound = 100;
    static constexpr int invalidUpperBound = -100;

    void SetUp()
    {
        randomVectorGenerator = std::make_shared<RandomVectorGenerator>();
    }

public:

    std::shared_ptr<RandomVectorGenerator> randomVectorGenerator;
};
