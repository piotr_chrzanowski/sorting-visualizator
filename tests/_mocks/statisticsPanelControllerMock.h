#pragma once
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "IStatisticsPanelController.h"


class StatisticsPanelControllerMock : public IStatisticsPanelController
{
public:
    MOCK_METHOD(void, updateStatisticsInfo, (StatisticsInfo), (override));

};
