#pragma once
#include "IChartController.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class ChartControllerMock : public IChartController
{
public:
    MOCK_METHOD(void, update, (), (override));
    MOCK_METHOD(void, swapBars, (size_t firstIndex, size_t secondIndex), (override));
    MOCK_METHOD(void, selectBar, (size_t barIndex), (override));
    MOCK_METHOD(void, deselectAllBars, (), (override));
    MOCK_METHOD(void, colorSelectedBars, (QColor color), (override));
    MOCK_METHOD(void, generateBars, (std::vector<int> & vector), (override));
};