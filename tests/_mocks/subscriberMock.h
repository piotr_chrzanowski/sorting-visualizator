#pragma once
#include "ISubscriber.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class SubscriberMock : public ISubscriber
{
public:
    MOCK_METHOD(void, update, (Notification), (override)); 
};
