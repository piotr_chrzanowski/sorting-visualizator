#pragma once
#include "ISortingController.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class SortingControllerMock : public ISortingController
{
public:
    MOCK_METHOD(void, sort, (std::vector<int> &vector), (override));
    MOCK_METHOD(bool, setSortingAlgorithm, (Algorithms algorithm), (override));
    MOCK_METHOD(bool, setSortingAlgorithm, (size_t algorithm), (override));
    MOCK_METHOD(Algorithms, getSelectedAlgorithm, (), (override));
};
