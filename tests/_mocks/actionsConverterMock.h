#pragma once
#include "IActionsConverter.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class ActionsConverterMock : public IActionsConverter
{
public:
    MOCK_METHOD(void, init, (), (override));
    MOCK_METHOD(void, clearActions, (), (override));
    MOCK_METHOD(bool, nextAction, (), (override));
    MOCK_METHOD(bool, previousAction, (), (override));
    MOCK_METHOD(size_t, getActionsCount, (), (override));
    MOCK_METHOD(void, generate, (std::vector<int> & pointsVector), (override));
    MOCK_METHOD(void, update, (Notification), (override)); 
};
