#pragma once
#include "ITimer.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class TimerMock : public ITimer
{
public:
    MOCK_METHOD(void, start, (), (override));
    MOCK_METHOD(void, stop, (), (override));
    MOCK_METHOD(bool, isTimerStarted, (), (override));
    MOCK_METHOD(void, setNewInterval, (size_t), (override));
    MOCK_METHOD(void, setTimeoutCallback, (ITimer::TimerCallback_t), (override));
};
