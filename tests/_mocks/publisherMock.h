#pragma once
#include "IPublisher.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class PublisherMock : public IPublisher
{
public:
    MOCK_METHOD(bool, subscribe, (std::shared_ptr<ISubscriber> subscriber), (override));
    MOCK_METHOD(bool, unsubscribe, (std::shared_ptr<ISubscriber> subscriber), (override));
    MOCK_METHOD(void, notify, (Notification), (override));
};
