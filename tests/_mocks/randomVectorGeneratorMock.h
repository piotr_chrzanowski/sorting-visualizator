#pragma once
#include "IRandomVectorGenerator.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class RandomVectorGeneratorMock : public IRandomVectorGenerator
{
public:
    MOCK_METHOD(void, generate, (size_t pointNumber, int lowerBound, int upperBound, std::vector<int> &generatedVector), (override));
};
