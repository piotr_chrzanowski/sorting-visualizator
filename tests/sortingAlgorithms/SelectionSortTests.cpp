#include "SortingAlgorithmsTestFixture.h"

TEST_F(SortingAlgorithmsFixture, SelectionSort_Should_Sort_All_Test_Cases)
{
    selectionSort->sort(testCase1);
    selectionSort->sort(testCase2);
    selectionSort->sort(testCase3);
    selectionSort->sort(testCase4);

    EXPECT_EQ(testCase1, sortedTestCase1);
    EXPECT_EQ(testCase2, sortedTestCase2);
    EXPECT_EQ(testCase3, sortedTestCase3);
    EXPECT_EQ(testCase4, sortedTestCase4);
}
