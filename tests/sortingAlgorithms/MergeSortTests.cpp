#include "SortingAlgorithmsTestFixture.h"

TEST_F(SortingAlgorithmsFixture, MergeSort_Should_Sort_All_Test_Cases)
{
    mergeSort->sort(testCase1);
    mergeSort->sort(testCase2);
    mergeSort->sort(testCase3);
    mergeSort->sort(testCase4);

    EXPECT_EQ(testCase1, sortedTestCase1);
    EXPECT_EQ(testCase2, sortedTestCase2);
    EXPECT_EQ(testCase3, sortedTestCase3);
    EXPECT_EQ(testCase4, sortedTestCase4);
}
