#pragma once
#include "SortingController.h"
#include "publisherMock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <memory>

using namespace ::testing;

class SortingControllerFixture : public testing::Test
{
public:
    void SetUp()
    {
        publisherMock = std::make_shared<PublisherMock>();
        sortingController = std::make_shared<SortingController>(publisherMock);
    }

public:
    std::shared_ptr<ISortingController> sortingController;
    std::shared_ptr<PublisherMock> publisherMock;
};
