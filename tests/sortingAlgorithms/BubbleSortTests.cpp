#include "SortingAlgorithmsTestFixture.h"

TEST_F(SortingAlgorithmsFixture, BubbleSort_Should_Sort_All_Test_Cases)
{
    bubbleSort->sort(testCase1);
    bubbleSort->sort(testCase2);
    bubbleSort->sort(testCase3);
    bubbleSort->sort(testCase4);

    EXPECT_EQ(testCase1, sortedTestCase1);
    EXPECT_EQ(testCase2, sortedTestCase2);
    EXPECT_EQ(testCase3, sortedTestCase3);
    EXPECT_EQ(testCase4, sortedTestCase4);
}
