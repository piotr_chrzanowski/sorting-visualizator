#include "SortingAlgorithmsTestFixture.h"

TEST_F(SortingAlgorithmsFixture, QuickSort_Should_Sort_All_Test_Cases)
{
    quickSort->sort(testCase1);
    quickSort->sort(testCase2);
    quickSort->sort(testCase3);
    quickSort->sort(testCase4);

    EXPECT_EQ(testCase1, sortedTestCase1);
    EXPECT_EQ(testCase2, sortedTestCase2);
    EXPECT_EQ(testCase3, sortedTestCase3);
    EXPECT_EQ(testCase4, sortedTestCase4);
}
