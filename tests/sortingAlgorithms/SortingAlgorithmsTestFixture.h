#pragma once
#include "BubbleSort.h"
#include "MergeSort.h"
#include "SelectionSort.h"
#include "QuickSort.h"
#include "SortingActionsPublisher.h"
#include "ISortingAlgorithm.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <memory>

using namespace ::testing;

class SortingAlgorithmsFixture : public testing::Test
{
public:
    void SetUp()
    {
        publisher = std::make_shared<SortingActionsPublisher>();
        bubbleSort = std::make_shared<BubbleSort>(publisher);
        selectionSort = std::make_shared<SelectionSort>(publisher);
        mergeSort = std::make_shared<MergeSort>(publisher);
        quickSort = std::make_shared<QuickSort>(publisher);

        testCase1 = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        testCase2 = {-1, -2, -3, -4, -5, -6, -7, -8, -9, -10};
        testCase3 = {9999999, 4, 3, 2, 1, 0};
        testCase4 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

public:
    std::shared_ptr<IPublisher> publisher;
    std::shared_ptr<ISortingAlgorithm> bubbleSort;
    std::shared_ptr<ISortingAlgorithm> selectionSort;
    std::shared_ptr<ISortingAlgorithm> mergeSort;
    std::shared_ptr<ISortingAlgorithm> quickSort;


    std::vector<int> testCase1;
    std::vector<int> sortedTestCase1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::vector<int> testCase2;
    std::vector<int> sortedTestCase2 = {-10, -9, -8, -7, -6, -5, -4, -3, -2, -1};

    std::vector<int> testCase3;
    std::vector<int> sortedTestCase3 = {0, 1, 2, 3, 4, 9999999};

    std::vector<int> testCase4;
    std::vector<int> sortedTestCase4 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
};
