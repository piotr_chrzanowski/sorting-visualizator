#include "SortingControllerTests.h"

TEST_F(SortingControllerFixture, Sorting_Controller_Should_Set_Selection_Sort_By_Default)
{
    EXPECT_EQ(sortingController->getSelectedAlgorithm(), Algorithms::SelectionSort);
}

TEST_F(SortingControllerFixture, Sorting_Controller_Should_Initialize_Map_With_Algorithms_Pointers_In_Construction)
{
    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::SelectionSort));
    EXPECT_EQ(sortingController->getSelectedAlgorithm(), Algorithms::SelectionSort);

    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::BubbleSort));
    EXPECT_EQ(sortingController->getSelectedAlgorithm(), Algorithms::BubbleSort);

    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::MergeSort));
    EXPECT_EQ(sortingController->getSelectedAlgorithm(), Algorithms::MergeSort);

    //EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::QuickSort));
    //EXPECT_EQ(sortingController->getSelectedAlgorithm(), Algorithms::QuickSort);
}

TEST_F(SortingControllerFixture, Sorting_Controller_Should_Change_Sorting_Algorithm_Based_On_Incoming_Integer)
{

    EXPECT_TRUE(sortingController->setSortingAlgorithm(0));
    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::SelectionSort));

    EXPECT_TRUE(sortingController->setSortingAlgorithm(1));
    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::BubbleSort));

    EXPECT_TRUE(sortingController->setSortingAlgorithm(2));
    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::MergeSort));

    // EXPECT_TRUE(sortingController->setSortingAlgorithm(3));
    //  EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::QuickSort));
}

TEST_F(SortingControllerFixture, Sorting_Controller_Should_Not_Change_Sorting_Algorithm_When_String_Is_Invalid)
{
    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::SelectionSort));
    EXPECT_FALSE(sortingController->setSortingAlgorithm(10));
    EXPECT_TRUE(sortingController->setSortingAlgorithm(Algorithms::SelectionSort));
}
