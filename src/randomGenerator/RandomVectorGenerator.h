#pragma once
#include <vector>
#include "IRandomVectorGenerator.h"

class RandomVectorGenerator : public IRandomVectorGenerator
{
public:
    RandomVectorGenerator() = default;
    virtual ~RandomVectorGenerator() = default;
    
    virtual void generate(size_t pointNumber, int lowerBound, int upperBound, std::vector<int> & vector) override;

private:
    bool boundsValid(int lowerBound, int upperBound);

};