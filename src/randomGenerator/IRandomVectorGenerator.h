#pragma once
#include <vector>

class IRandomVectorGenerator
{
public:
    IRandomVectorGenerator() = default;
    virtual ~IRandomVectorGenerator() = default;

    virtual void generate(size_t pointNumber, int lowerBound, int upperBound, std::vector<int> &generatedVector) = 0;
};