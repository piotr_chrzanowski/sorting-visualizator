#include "RandomVectorGenerator.h"
#include <random>

void RandomVectorGenerator::generate(size_t pointNumber, int lowerBound, int upperBound, std::vector<int> &generatedVector)
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(lowerBound,upperBound);

    if (boundsValid(lowerBound, upperBound))
    {
        generatedVector.resize(pointNumber);

        for (size_t i = 0; i < generatedVector.size(); ++i)
        {
            generatedVector.at(i) = dist(rng);
        }
    }
}

bool RandomVectorGenerator::boundsValid(int lowerBound, int upperBound)
{
    return lowerBound < upperBound;
}
