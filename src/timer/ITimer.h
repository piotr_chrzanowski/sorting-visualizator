#pragma once
#include <stdio.h>
#include <functional>

class ITimer
{
public:
    using TimerCallback_t = std::function<void()>;

    ITimer() = default;
    virtual ~ITimer() = default;
   
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual bool isTimerStarted() = 0;
    virtual void setNewInterval(size_t newInterval) = 0;
    virtual void setTimeoutCallback(TimerCallback_t callback) = 0;
};