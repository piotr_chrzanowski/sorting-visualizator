#pragma once
#include "ITimer.h"
#include <thread>
#include <chrono>
#include <memory>
#include <atomic>

class TimeoutTimer : public ITimer
{
public:
    static constexpr size_t defaultInterval_ms = 1;
    TimeoutTimer();
    virtual ~TimeoutTimer();
   
    virtual void start() override;
    virtual void stop() override;
    virtual bool isTimerStarted() override;
    virtual void setNewInterval(size_t newInterval) override;
    virtual void setTimeoutCallback(TimerCallback_t callback) override;

private:
    void threadLoop();

private:
    std::atomic_bool m_isTimerRunning;
    std::thread m_timerThread;
    std::chrono::milliseconds m_timerInterval;
    TimerCallback_t m_timerCallback;
};