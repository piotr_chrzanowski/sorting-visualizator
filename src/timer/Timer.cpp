#include "Timer.h"

TimeoutTimer::TimeoutTimer()
    : ITimer()
    , m_isTimerRunning(false)
    , m_timerInterval(std::chrono::milliseconds(defaultInterval_ms))
{
}

TimeoutTimer::~TimeoutTimer()
{
}

void TimeoutTimer::start()
{
    if (!m_isTimerRunning)
    {
        m_isTimerRunning = true;
        m_timerThread = std::thread(std::bind(&TimeoutTimer::threadLoop, this));
    }
}

void TimeoutTimer::stop()
{
    m_isTimerRunning = false;
    m_timerThread.join();

}

bool TimeoutTimer::isTimerStarted()
{
    return m_isTimerRunning;
}

void TimeoutTimer::setNewInterval(size_t newInterval)
{
    m_timerInterval = std::chrono::milliseconds(newInterval);
}

void TimeoutTimer::setTimeoutCallback(TimerCallback_t callback)
{
    m_timerCallback = callback;
}

void TimeoutTimer::threadLoop()
{
    while (m_isTimerRunning)
    {
        std::this_thread::sleep_for(m_timerInterval);

        if (m_timerCallback)
        {
            m_timerCallback();
        }
    }

}
