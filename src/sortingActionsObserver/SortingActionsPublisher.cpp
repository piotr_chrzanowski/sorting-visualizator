#include "SortingActionsPublisher.h"
#include <algorithm>

SortingActionsPublisher::SortingActionsPublisher()
    : IPublisher()
    , m_subscribers()
{
}

bool SortingActionsPublisher::subscribe(std::shared_ptr<ISubscriber> subscriber)
{
    auto it = std::find(m_subscribers.begin(), m_subscribers.end(), subscriber);

    if (it == m_subscribers.end())
    {
        m_subscribers.push_back(subscriber);
        return true;
    }

    return false;
}

bool SortingActionsPublisher::unsubscribe(std::shared_ptr<ISubscriber> subscriber)
{
    auto it = std::find(m_subscribers.begin(), m_subscribers.end(), subscriber);

    if (it != m_subscribers.end())
    {
        m_subscribers.erase(it);
        return true;
    }

    return false;
}

void SortingActionsPublisher::notify(Notification message)
{
    for (auto &subscriber : m_subscribers)
    {
        subscriber->update(message);
    }
}
