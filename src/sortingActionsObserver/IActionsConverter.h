#pragma once
#include "ISubscriber.h"
#include <stdio.h>
#include <vector>

class IActionsConverter : public ISubscriber
{
public:
    IActionsConverter() = default;
    virtual ~IActionsConverter() = default;

    virtual void init() = 0;
    virtual void clearActions() = 0;
    virtual bool nextAction() = 0;
    virtual bool previousAction() = 0;
    virtual size_t getActionsCount() = 0;
    virtual void generate(std::vector<int> &pointsVector) = 0;
    virtual void update(Notification message) = 0;
};