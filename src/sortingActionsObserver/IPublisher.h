#pragma once
#include "Notifications.h"
#include "ISubscriber.h"
#include <memory>

class IPublisher
{
public:
    IPublisher() = default;
    virtual ~IPublisher() = default;

public:
    virtual bool subscribe(std::shared_ptr<ISubscriber> subscriber) = 0;
    virtual bool unsubscribe(std::shared_ptr<ISubscriber> subscriber) = 0;
    virtual void notify(Notification message) = 0;
};