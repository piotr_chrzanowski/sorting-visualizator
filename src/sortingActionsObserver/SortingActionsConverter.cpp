#include "SortingActionsConverter.h"
#include <iostream>

SortingActionsConverter::SortingActionsConverter(std::shared_ptr<IPublisher> publisher, std::shared_ptr<IChartController> chartController, std::shared_ptr<IStatisticsPanelController> panelController)
    : IActionsConverter()
    , m_publisher(publisher)
    , m_chartController(chartController)
    , m_panelController(panelController)
    , m_sortingActions()
    , m_currentAction(-1)
    , m_swapCount(0)
    , m_comparisonCount(0)
    , m_minimumSelectionCount(0)
{
}

void SortingActionsConverter::init()
{
    if (m_publisher)
    {
        m_publisher->subscribe(shared_from_this());
    }
}

void SortingActionsConverter::clearActions()
{
    m_currentAction = 0;
    m_sortingActions.clear();
    m_statisticsInfo.clear();
    m_swapCount = 0;
    m_comparisonCount = 0;
    m_panelController->updateStatisticsInfo(StatisticsInfo{m_swapCount, m_comparisonCount});
    m_chartController->deselectAllBars();

}

bool SortingActionsConverter::nextAction()
{
    if ((m_currentAction + 1) < static_cast<int>(m_sortingActions.size()))
    {
        processAction(++m_currentAction);
        return true;
    }

    return false;
}

bool SortingActionsConverter::previousAction()
{
    if ((m_currentAction - 1) >= 0)
    {
        processAction(--m_currentAction);
        return true;
    }

    return false;
}

void SortingActionsConverter::update(Notification message)
{
    m_sortingActions.push_back(message);

    if (message.type == NotificationType::Swap)
    {
        m_statisticsInfo.push_back(StatisticsInfo{.swapCount = ++m_swapCount, .comparisonCount = m_comparisonCount});

    } else if (message.type == NotificationType::Comparison)
    {
        m_statisticsInfo.push_back(StatisticsInfo{.swapCount = m_swapCount, .comparisonCount = ++m_comparisonCount});
    } else
    {
        m_statisticsInfo.push_back(StatisticsInfo{.swapCount = m_swapCount, .comparisonCount = m_comparisonCount});
    }
}

size_t SortingActionsConverter::getActionsCount()
{
    return m_sortingActions.size();
}

void SortingActionsConverter::generate(std::vector<int> &pointsVector)
{
    m_chartController->generateBars(pointsVector);
}

void SortingActionsConverter::processAction(size_t actionID)
{
    auto action = m_sortingActions[actionID];

    m_chartController->deselectAllBars();

    switch (action.type)
    {
        case NotificationType::Comparison:
        {
            m_chartController->selectBar(action.idxFirst);
            m_chartController->selectBar(action.idxSecond);
            m_chartController->colorSelectedBars(QColor(Qt::green));
            break;
        }
        case NotificationType::Swap:
        {
            m_chartController->swapBars(action.idxFirst, action.idxSecond);
            break;
        }
        case NotificationType::SwapComparison:
        {
            m_chartController->selectBar(action.idxFirst);
            m_chartController->selectBar(action.idxSecond);
            m_chartController->colorSelectedBars(QColor(Qt::red));
            break;
        }

        default:;
    }

    m_panelController->updateStatisticsInfo(m_statisticsInfo[actionID]);
}
