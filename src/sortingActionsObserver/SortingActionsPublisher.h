#pragma once
#include "IPublisher.h"
#include <vector>

class SortingActionsPublisher : public IPublisher
{
public:
    SortingActionsPublisher();
    virtual ~SortingActionsPublisher() = default;

public:
    virtual bool subscribe(std::shared_ptr<ISubscriber> subscriber) override;
    virtual bool unsubscribe(std::shared_ptr<ISubscriber> subscriber) override;
    virtual void notify(Notification message) override;

private:
    std::vector<std::shared_ptr<ISubscriber>> m_subscribers;
};
