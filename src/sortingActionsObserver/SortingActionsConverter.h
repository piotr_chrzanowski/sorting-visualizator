#pragma once
#include "IActionsConverter.h"
#include "IChartController.h"
#include "IPublisher.h"
#include "ISubscriber.h"
#include "StatisticsPanelController.h"
#include <list>
#include <memory>

class SortingActionsConverter : public IActionsConverter, public std::enable_shared_from_this<SortingActionsConverter>
{
public:
    SortingActionsConverter(std::shared_ptr<IPublisher> publisher, std::shared_ptr<IChartController> chartController, std::shared_ptr<IStatisticsPanelController> panelController);
    virtual ~SortingActionsConverter() = default;

    virtual void update(Notification message) override;

    virtual void init() override;
    virtual void clearActions() override;
    virtual bool nextAction() override;
    virtual bool previousAction() override;
    virtual size_t getActionsCount() override;

    virtual void generate(std::vector<int> &pointsVector) override;

private:
    void processAction(size_t actionID);

private:
    std::shared_ptr<IPublisher> m_publisher;
    std::shared_ptr<IChartController> m_chartController;
    std::shared_ptr<IStatisticsPanelController> m_panelController;
    std::vector<Notification> m_sortingActions;
    std::vector<StatisticsInfo> m_statisticsInfo;

    int m_currentAction;

    size_t m_swapCount;
    size_t m_comparisonCount;
    size_t m_minimumSelectionCount;
};