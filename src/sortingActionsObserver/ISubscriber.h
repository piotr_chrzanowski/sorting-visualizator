#pragma once
#include <stdio.h>
#include <string>
#include "Notifications.h"
#include <vector>


class ISubscriber
{
public:
    ISubscriber() = default;
    virtual ~ISubscriber() = default;

public:
    virtual void update(Notification message) = 0;
};

