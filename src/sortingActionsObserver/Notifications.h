#pragma once

enum class NotificationType
{
    Comparison,
    SwapComparison,
    Swap,
    CopyComparison,
    Copy
};

struct Notification
{
    NotificationType type;
    int idxFirst;
    int idxSecond;
};


enum class Algorithms
{
    SelectionSort,
    BubbleSort,
    MergeSort,
    QuickSort
};

