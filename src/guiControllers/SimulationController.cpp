#include "SimulationController.h"

SimulationController::SimulationController(std::shared_ptr<ISortingController> sortingController, std::shared_ptr<IActionsConverter> sortingActionsConverter, std::shared_ptr<IRandomVectorGenerator> rvg, std::shared_ptr<ITimer> timer, QObject *parent)
    : m_isStartLocked(false)
    , m_isStopLocked(true)
    , m_isNextLocked(false)
    , m_isPreviousLocked(false)
    , m_isResetLocked(false)
    , m_isPointsSliderLocked(false)
    , m_isAlgorithmBoxLocked(false)
    , m_sortingController(sortingController)
    , m_sortingActionsConverter(sortingActionsConverter)
    , m_rvg(rvg)
    , m_timer(timer)
    , m_timeInterval(baseTimeInterval)
    , m_barsVector()
    , m_unsortedBarsVector()
{
    if (m_rvg == nullptr)
    {
        m_rvg = std::make_shared<RandomVectorGenerator>();
    }

    if (m_timer == nullptr)
    {
        m_timer = std::make_shared<TimeoutTimer>();
        m_timer->setNewInterval(m_timeInterval);
        m_timer->setTimeoutCallback(std::bind(&SimulationController::timeout, this));
    }
}

void SimulationController::init()
{
    m_rvg->generate(barsSize, barLowerBound, barUpperBound, m_barsVector);
    m_sortingActionsConverter->init();
    m_sortingActionsConverter->generate(m_barsVector);
    m_unsortedBarsVector = m_barsVector;
    m_sortingController->sort(m_barsVector);
}

void SimulationController::start()
{
    m_isStartLocked = true;
    m_isNextLocked = true;
    m_isPreviousLocked = true;
    m_isResetLocked = true;

    m_isPointsSliderLocked = true;
    m_isAlgorithmBoxLocked = true;

    m_isStopLocked = false;

    emit startLockedChanged();
    emit stopLockedChanged();
    emit nextLockedChanged();
    emit previousLockedChanged();
    emit resetLockedChanged();
    emit pointsSliderLockedChanged();
    emit algorithmBoxLockedChanged();

    m_timer->start();
}

void SimulationController::stop()
{
    m_isStartLocked = false;
    m_isNextLocked = false;
    m_isPreviousLocked = false;
    m_isResetLocked = false;

    m_isPointsSliderLocked = false;
    m_isAlgorithmBoxLocked = false;

    m_isStopLocked = true;

    emit startLockedChanged();
    emit stopLockedChanged();
    emit nextLockedChanged();
    emit previousLockedChanged();
    emit resetLockedChanged();
    emit pointsSliderLockedChanged();
    emit algorithmBoxLockedChanged();

    m_timer->stop();
}

void SimulationController::next()
{
    m_sortingActionsConverter->nextAction();
}

void SimulationController::previous()
{
    m_sortingActionsConverter->previousAction();
}

void SimulationController::reset()
{
    m_sortingActionsConverter->clearActions();
    m_sortingActionsConverter->generate(m_unsortedBarsVector);
    m_barsVector = m_unsortedBarsVector;
    m_sortingController->sort(m_barsVector);
}

void SimulationController::setSimulationPointsNumber(size_t pointsNumber)
{
    m_rvg->generate(pointsNumber, SimulationController::barLowerBound, SimulationController::barUpperBound, m_barsVector);
    m_sortingActionsConverter->clearActions();
    m_sortingActionsConverter->generate(m_barsVector);
    m_unsortedBarsVector = m_barsVector;
    m_sortingController->sort(m_barsVector);
}

void SimulationController::setSimulationTimeInterval(size_t newInterval)
{
    m_timeInterval = 1000/(newInterval + 1) + minimalTimeInterval;
    m_timer->setNewInterval(m_timeInterval);
}

void SimulationController::setSortingAlgorithm(size_t algorithm)
{
    m_sortingActionsConverter->clearActions();
    m_sortingController->setSortingAlgorithm(algorithm);
    m_sortingController->sort(m_barsVector);
}

void SimulationController::timeout()
{
    m_sortingActionsConverter->nextAction();
}