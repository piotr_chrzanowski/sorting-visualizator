#pragma once
#include "ISimulationController.h"
#include "ISortingController.h"
#include "IActionsConverter.h"
#include "RandomVectorGenerator.h"
#include "Timer.h"
#include <QObject>
#include <memory>

class SimulationController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isStartLocked READ isStartLocked NOTIFY startLockedChanged)
    Q_PROPERTY(bool isStopLocked READ isStopLocked NOTIFY stopLockedChanged)
    Q_PROPERTY(bool isNextLocked READ isNextLocked NOTIFY nextLockedChanged)
    Q_PROPERTY(bool isPreviousLocked READ isPreviousLocked NOTIFY previousLockedChanged)
    Q_PROPERTY(bool isResetLocked READ isResetLocked NOTIFY resetLockedChanged)


    Q_PROPERTY(bool isPointsSliderLocked READ isPointsSliderLocked NOTIFY pointsSliderLockedChanged)
    Q_PROPERTY(bool isAlgorithmBoxLocked READ isAlgorithmBoxLocked NOTIFY algorithmBoxLockedChanged)

public:
    static constexpr size_t barsSize = 10;
    static constexpr size_t barLowerBound = 0;
    static constexpr size_t barUpperBound = 100;
    static constexpr size_t baseTimeInterval = 1000;
    static constexpr size_t minimalTimeInterval = 10;


public:
    explicit SimulationController(std::shared_ptr<ISortingController> sortingController, std::shared_ptr<IActionsConverter> sortingActionsConverter, std::shared_ptr<IRandomVectorGenerator> rvg = nullptr, std::shared_ptr<ITimer> timer = nullptr, QObject *parent = nullptr);
    virtual ~SimulationController() = default;

public:
    bool isStartLocked() const { return m_isStartLocked; }
    bool isStopLocked() const { return m_isStopLocked; }
    bool isNextLocked() const { return m_isNextLocked; }
    bool isPreviousLocked() const { return m_isPreviousLocked; }
    bool isResetLocked() const { return m_isResetLocked; }
    bool isPointsSliderLocked() const { return m_isPointsSliderLocked; }
    bool isAlgorithmBoxLocked() const { return m_isAlgorithmBoxLocked; }
    size_t getSimulationTimeInterval() const { return m_timeInterval; }

signals:
    void startLockedChanged();
    void stopLockedChanged();
    void nextLockedChanged();
    void previousLockedChanged();
    void resetLockedChanged();
    void pointsSliderLockedChanged();
    void algorithmBoxLockedChanged();

public slots:
    void init();
    void start();
    void stop();
    void next();
    void previous();
    void reset();
    void setSimulationPointsNumber(size_t pointsNumber);
    void setSimulationTimeInterval(size_t newInterval);
    void setSortingAlgorithm(size_t algorithm);
    void timeout();

private:
    bool m_isStartLocked;
    bool m_isStopLocked;
    bool m_isNextLocked;
    bool m_isPreviousLocked;
    bool m_isResetLocked;
    bool m_isPointsSliderLocked;
    bool m_isAlgorithmBoxLocked;

    std::shared_ptr<ISortingController> m_sortingController;
    std::shared_ptr<IActionsConverter> m_sortingActionsConverter;
    std::shared_ptr<IRandomVectorGenerator> m_rvg;
    std::shared_ptr<ITimer> m_timer;

    size_t m_timeInterval;
    std::vector<int> m_barsVector;
    std::vector<int> m_unsortedBarsVector;

};