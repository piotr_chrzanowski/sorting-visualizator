#pragma once
#include <QColor>
#include <vector>

class IChartController
{
public:
    IChartController() = default;
    virtual ~IChartController() = default;

    virtual void update() = 0;
    virtual void swapBars(size_t firstIndex, size_t secondIndex) = 0;
    virtual void selectBar(size_t barIndex) = 0;
    virtual void deselectAllBars() = 0;
    virtual void colorSelectedBars(QColor color) = 0;
    virtual void generateBars(std::vector<int> & vector) = 0;
};