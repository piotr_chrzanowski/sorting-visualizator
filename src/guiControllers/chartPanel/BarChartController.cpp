#include "BarChartController.h"

BarChartController::BarChartController(QObject *parent)
    : isAxisAttached(false)
{
    m_categorySeries = std::make_shared<QBarCategoryAxis>(this);
    m_set = nullptr;
    m_list = nullptr;
}

void BarChartController::connect(QAbstractBarSeries *barSeries)
{
    if (barSeries)
    {
        m_barSeries = std::shared_ptr<QBarSeries>(static_cast<QBarSeries *>(barSeries));
        m_chart = std::shared_ptr<QChart>(m_barSeries->chart());
    }
}

void BarChartController::update()
{
    m_barSeries->take(m_set);
    m_barSeries->append(m_set);
}

void BarChartController::swapBars(size_t firstIndex, size_t secondIndex)
{
    if (firstIndex != secondIndex)
    {
        auto tmp = m_set->at(firstIndex);
        m_set->replace(firstIndex, m_set->at(secondIndex));
        m_set->replace(secondIndex, tmp);
    }
}

void BarChartController::selectBar(size_t barIndex)
{
    m_set->selectBar(barIndex);
}

void BarChartController::deselectAllBars()
{
    m_set->deselectAllBars();
}

void BarChartController::colorSelectedBars(QColor color)
{
    m_set->setSelectedColor(color);
}

void BarChartController::generateBars(std::vector<int> &vector)
{
    m_categorySeries->clear();
    m_barSeries->clear();

    m_set = new QBarSet("Data");
    m_list = new QStringList;

    for (size_t i = 0; i < vector.size(); ++i)
    {
        m_set->insert(i, vector.at(i));
        m_list->insert(i, QString::number(i));
    }

    if (!isAxisAttached)
    {
        m_chart->addAxis(m_categorySeries.get(), Qt::AlignBottom);
        m_barSeries->attachAxis(m_categorySeries.get());
        isAxisAttached = true;
    }

    m_barSeries->append(m_set);
    m_categorySeries->append(*m_list);

    update();
}
