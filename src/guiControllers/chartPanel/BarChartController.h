#pragma once
#include "IChartController.h"
#include <QAbstractBarSeries>
#include <QBarCategoryAxis>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>
#include <QColor>
#include <QList>
#include <QObject>
#include <memory>

class BarChartController : public QObject, public IChartController
{
    Q_OBJECT
public:
    explicit BarChartController(QObject *parent = nullptr);
    ~BarChartController() = default;

    virtual void update() override;
    virtual void swapBars(size_t firstIndex, size_t secondIndex) override;
    virtual void selectBar(size_t barIndex) override;
    virtual void deselectAllBars() override;
    virtual void colorSelectedBars(QColor color) override;
    virtual void generateBars(std::vector<int> &vector) override;

signals:

public slots:
    void connect(QAbstractBarSeries *barSeries);

private:
    bool isAxisAttached;
    std::shared_ptr<QChart> m_chart;
    std::shared_ptr<QBarSeries> m_barSeries;
    std::shared_ptr<QBarCategoryAxis> m_categorySeries;

    QBarSet *m_set;
    QStringList *m_list;
};