#pragma once
#include <stdio.h>

class ISimulationController
{
public:
    ISimulationController() = default;
    virtual ~ISimulationController() = default;

    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void next() = 0;
    virtual void previous() = 0;
    virtual void changeSimulationPointsNumber(size_t pointsNumber) = 0;
    virtual void changeSimulationTimeInterval(size_t newInterval) = 0;

};