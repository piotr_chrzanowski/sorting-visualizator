#pragma once
#include "IStatisticsPanelController.h"
#include <QObject>
#include <stdio.h>

class StatisticsPanelController : public QObject,  public IStatisticsPanelController
{
    Q_PROPERTY(size_t getComparisonCount READ getComparisonCount NOTIFY comparisonCountChanged)
    Q_PROPERTY(size_t getSwapCount READ getSwapCount NOTIFY swapCountChanged)
    Q_OBJECT

public:
    explicit StatisticsPanelController(QObject *parent = nullptr);
    virtual ~StatisticsPanelController() = default;

    virtual void updateStatisticsInfo(StatisticsInfo info) override;

signals:
    void comparisonCountChanged();
    void swapCountChanged();  

public:
    size_t getComparisonCount() { return m_info.comparisonCount; }
    size_t getSwapCount() { return m_info.swapCount; }

private:
    StatisticsInfo m_info;
};