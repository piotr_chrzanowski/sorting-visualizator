#include "StatisticsPanelController.h"

StatisticsPanelController::StatisticsPanelController(QObject *parent)
{
}

void StatisticsPanelController::updateStatisticsInfo(StatisticsInfo info)
{
    m_info = info;

    emit comparisonCountChanged();
    emit swapCountChanged();
}
