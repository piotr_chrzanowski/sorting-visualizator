#pragma once
#include <stdio.h>

struct StatisticsInfo
{
    size_t swapCount = 0;
    size_t comparisonCount = 0;
};

[[maybe_unused]] static bool operator==(const StatisticsInfo &lhs, const StatisticsInfo &rhs)
{
    bool status = false;
    status &= (lhs.comparisonCount == rhs.swapCount);
    status &= (lhs.comparisonCount == rhs.comparisonCount);
    return status;
}

class IStatisticsPanelController
{
public:
    IStatisticsPanelController() = default;
    virtual ~IStatisticsPanelController() = default;

    virtual void updateStatisticsInfo(StatisticsInfo info) = 0;
    ;
};