#pragma once
#include "ISortingAlgorithm.h"

class MergeSort : public ISortingAlgorithm
{
public:
    MergeSort(std::shared_ptr<IPublisher> publisher);

    virtual void sort(std::vector<int> &vector) override;

private:
    void mergeSort(std::vector<int> &vec, int l, int r);
    void merge(std::vector<int> &vec, int l, int m, int r);
};