#include "BubbleSort.h"

BubbleSort::BubbleSort(std::shared_ptr<IPublisher> publisher) : ISortingAlgorithm(publisher)
{
}

void BubbleSort::sort(std::vector<int> &vector)
{
    for (int i = 0; i < (static_cast<int>(vector.size() - 1)); ++i)
    {
        for (int j = 0; j < (static_cast<int>(vector.size() - 1 - i)); ++j)
        {
            m_publisher->notify(Notification{.type = NotificationType::Comparison, .idxFirst = j + 1, .idxSecond = j});

            if (vector.at(j + 1) < vector.at(j))
            {
                m_publisher->notify(Notification{.type = NotificationType::SwapComparison, .idxFirst = j + 1, .idxSecond = j});

                std::swap(vector.at(j + 1), vector.at(j));

                m_publisher->notify(Notification{.type = NotificationType::Swap, .idxFirst = j + 1, .idxSecond = j});
            }
        }
    }
}
