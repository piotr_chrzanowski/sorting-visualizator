#pragma once
#include "ISortingAlgorithm.h"

class BubbleSort : public ISortingAlgorithm
{
public:
    BubbleSort(std::shared_ptr<IPublisher> publisher);
    
    virtual void sort(std::vector<int> &vector) override;
};