#pragma once
#include "Notifications.h"
#include <vector>

class ISortingController
{
public:
    ISortingController() = default;
    virtual ~ISortingController() = default;

    virtual Algorithms getSelectedAlgorithm() = 0;
    virtual bool setSortingAlgorithm(Algorithms algorithm)  = 0;
    virtual bool setSortingAlgorithm(size_t algorithm) = 0;    
    virtual void sort(std::vector<int> &vector) = 0;
};