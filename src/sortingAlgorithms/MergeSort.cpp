#include "MergeSort.h"
#include <algorithm>

MergeSort::MergeSort(std::shared_ptr<IPublisher> publisher) : ISortingAlgorithm(publisher)
{
}

void MergeSort::sort(std::vector<int> &vector)
{
    int l = 0;
    int r = static_cast<int>(vector.size() - 1);

    mergeSort(vector, l, r);
}

void MergeSort::mergeSort(std::vector<int> &vec, int l, int r)
{
    if (l < r)
    {
        int m = l + (r - l) / 2;

        mergeSort(vec, l, m);

        mergeSort(vec, m + 1, r);

        merge(vec, l, m, r);
    }
}

void MergeSort::merge(std::vector<int> &vec, int l, int m, int r)
{
    int leftSize = m - l + 1;
    int rightSize = r - m;

    std::vector<int> Left(leftSize, 0);
    std::vector<int> Right(rightSize, 0);

    std::copy_n(vec.begin() + l, leftSize, Left.begin());
    std::copy_n(vec.begin() + m + 1, rightSize, Right.begin());

    int li = 0, ri = 0, n = l;

    while (li < leftSize && ri < rightSize)
    {   

        m_publisher->notify(Notification{.type = NotificationType::Comparison, .idxFirst = li + l, .idxSecond = ri + m + 1});
   
        if (Left.at(li) < Right.at(ri))
        {
            m_publisher->notify(Notification{.type = NotificationType::Copy, .idxFirst = n, .idxSecond = li + l});
            m_publisher->notify(Notification{.type = NotificationType::CopyComparison, .idxFirst = n, .idxSecond = li + l});

            vec.at(n) = Left.at(li);
            li++;
        } else
        {
            m_publisher->notify(Notification{.type = NotificationType::Copy, .idxFirst = n, .idxSecond = ri + m + 1});
            m_publisher->notify(Notification{.type = NotificationType::CopyComparison, .idxFirst = n, .idxSecond = ri + m + 1});
            vec.at(n) = Right.at(ri);
            ri++;
        }

        n++;
    }

    while (li < leftSize)
    {
        m_publisher->notify(Notification{.type = NotificationType::Copy, .idxFirst = n, .idxSecond = li + l});
        m_publisher->notify(Notification{.type = NotificationType::CopyComparison, .idxFirst = n, .idxSecond = li + l});
        vec.at(n) = Left.at(li);
        ++li;
        ++n;
    }

    while (ri < rightSize)
    {
        m_publisher->notify(Notification{.type = NotificationType::Copy, .idxFirst = n, .idxSecond = ri + m + 1});
        m_publisher->notify(Notification{.type = NotificationType::CopyComparison, .idxFirst = n, .idxSecond = ri + m + 1});
        vec.at(n) = Right.at(ri);
        ++ri;
        ++n;
    }
}
