#pragma once
#include "ISortingAlgorithm.h"

class QuickSort : public ISortingAlgorithm
{
public:
    QuickSort(std::shared_ptr<IPublisher> publisher);
    virtual ~QuickSort() = default;

    virtual void sort(std::vector<int> &vector) override;

private:
    void quickSort(std::vector<int> &vector, int low, int h);
    size_t partition(std::vector<int> &vector, int low, int h);
};