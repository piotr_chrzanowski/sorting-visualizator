#include "SortingController.h"
#include "BubbleSort.h"
#include "MergeSort.h"
#include "QuickSort.h"
#include "SelectionSort.h"
#include <utility>

SortingController::SortingController(std::shared_ptr<IPublisher> publisher)
    : ISortingController()
    , m_selectedAlgorithm(Algorithms::SelectionSort)
    , m_publisher(publisher)
    , m_algorithms()
{
    std::shared_ptr<ISortingAlgorithm> selectionSort = std::make_shared<SelectionSort>(m_publisher);
    std::shared_ptr<ISortingAlgorithm> bubbleSort = std::make_shared<BubbleSort>(m_publisher);
    std::shared_ptr<ISortingAlgorithm> mergeSort = std::make_shared<MergeSort>(m_publisher);
    std::shared_ptr<ISortingAlgorithm> quickSort = std::make_shared<QuickSort>(m_publisher);


    m_algorithms.insert(std::make_pair(Algorithms::SelectionSort, selectionSort));
    m_algorithms.insert(std::make_pair(Algorithms::BubbleSort, bubbleSort));
    m_algorithms.insert(std::make_pair(Algorithms::MergeSort, mergeSort));
    m_algorithms.insert(std::make_pair(Algorithms::QuickSort, quickSort));

}

Algorithms SortingController::getSelectedAlgorithm()
{
    return m_selectedAlgorithm;
}

bool SortingController::setSortingAlgorithm(Algorithms algorithm)
{
    if (m_algorithms.count(algorithm))
    {
        m_selectedAlgorithm = algorithm;
        return true;
    }

    return false;
}

bool SortingController::setSortingAlgorithm(size_t algorithm)
{
    switch (algorithm)
    {
        case 0:
        {
            setSortingAlgorithm(Algorithms::SelectionSort);
            break;
        }
        case 1:
        {
            setSortingAlgorithm(Algorithms::BubbleSort);
            break;
        }
        case 2:
        {
            setSortingAlgorithm(Algorithms::QuickSort);
            break;
        }
        case 3:
        {
            setSortingAlgorithm(Algorithms::MergeSort);
            break;
        }
        default:
            return false;
    }

    return true;
}

void SortingController::sort(std::vector<int>& vector)
{
    m_algorithms[m_selectedAlgorithm]->sort(vector);
}
