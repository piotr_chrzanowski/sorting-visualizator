#include "QuickSort.h"

QuickSort::QuickSort(std::shared_ptr<IPublisher> publisher)
    : ISortingAlgorithm(publisher){};

void QuickSort::sort(std::vector<int> &vector)
{
    quickSort(vector, 0, vector.size() - 1);
}

void QuickSort::quickSort(std::vector<int> &vector, int low, int high)
{
    if (low < high)
    {
        size_t pivotIndex = partition(vector, low, high);

        quickSort(vector, low, pivotIndex - 1);
        quickSort(vector, pivotIndex + 1, high);
    }
}

size_t QuickSort::partition(std::vector<int> &vector, int low, int high)
{

    int pivot = vector.at(high);
    int i = low - 1;

    for (int j = low; j <= (high - 1); ++j)
    {
        m_publisher->notify(Notification{.type = NotificationType::Comparison, .idxFirst = j, .idxSecond = high});

        if (vector.at(j) < pivot)
        {

            ++i;
            std::swap(vector.at(j), vector.at(i));

            m_publisher->notify(Notification{.type = NotificationType::SwapComparison, .idxFirst = j, .idxSecond = i});
            m_publisher->notify(Notification{.type = NotificationType::Swap, .idxFirst = j, .idxSecond = i});
        }
    }

    std::swap(vector.at(i + 1), vector.at(high));

    m_publisher->notify(Notification{.type = NotificationType::SwapComparison, .idxFirst = i + 1, .idxSecond = high});
    m_publisher->notify(Notification{.type = NotificationType::Swap, .idxFirst = i + 1, .idxSecond = high});

    return i + 1;
}
