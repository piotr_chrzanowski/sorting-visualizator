#include "SelectionSort.h"

SelectionSort::SelectionSort(std::shared_ptr<IPublisher> publisher)
    : ISortingAlgorithm(publisher)
{
}

void SelectionSort::sort(std::vector<int> &vector)
{
    int minimumIndex = 0;

    for (int i = 0; i < (static_cast<int>(vector.size() - 1)); ++i)
    {
        minimumIndex = i;

        for (int j = i + 1; j < static_cast<int>(vector.size()); ++j)
        {
            m_publisher->notify(Notification{.type = NotificationType::Comparison, .idxFirst = j, .idxSecond = minimumIndex});
            if (vector.at(j) < vector.at(minimumIndex))
            {
                minimumIndex = j;
            }
        }

        std::swap(vector.at(i), vector.at(minimumIndex));

        m_publisher->notify(Notification{.type = NotificationType::SwapComparison, .idxFirst = i, .idxSecond = minimumIndex});
        m_publisher->notify(Notification{.type = NotificationType::Swap, .idxFirst = i, .idxSecond = minimumIndex});
    }
}
