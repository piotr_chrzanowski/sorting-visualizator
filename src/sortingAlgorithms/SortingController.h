#pragma once
#include "IPublisher.h"
#include "ISortingAlgorithm.h"
#include "ISortingController.h"
#include <unordered_map>
#include <vector>
#include <string>

class SortingController : public ISortingController
{
public:
    SortingController(std::shared_ptr<IPublisher> publisher);
    virtual ~SortingController() = default;

    virtual Algorithms getSelectedAlgorithm() override;
    virtual bool setSortingAlgorithm(Algorithms algorithm) override;
    virtual bool setSortingAlgorithm(size_t algorithm) override;    
    virtual void sort(std::vector<int>& vector) override;

private:
    Algorithms m_selectedAlgorithm;
    std::shared_ptr<IPublisher> m_publisher;
    std::unordered_map<Algorithms, std::shared_ptr<ISortingAlgorithm>> m_algorithms;
};