#pragma once
#include "ISortingAlgorithm.h"

class SelectionSort : public ISortingAlgorithm
{
public:
    SelectionSort(std::shared_ptr<IPublisher> publisher);
    
    virtual void sort(std::vector<int> &vector) override;
};