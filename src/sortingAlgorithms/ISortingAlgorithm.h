#pragma once
#include "IPublisher.h"
#include <vector>

class ISortingAlgorithm
{
public:
    ISortingAlgorithm(std::shared_ptr<IPublisher> publisher) : m_publisher(publisher) {};
    virtual ~ISortingAlgorithm() = default;

    virtual void sort(std::vector<int> &vector) = 0;

protected:
    std::shared_ptr<IPublisher> m_publisher;
};