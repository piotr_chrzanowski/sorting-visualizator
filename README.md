# Sorting Visualizator

## What is it?

Sorting visualizator is a GUI application which can be used to visualize, explore and learn about sorting process of popular sorting algorithms like <b>selection sort</b>, <b>bubble sort</b>, <b>quick sort</b> etc.

## Features
- <b> Sorting visualizing</b>
- <b> Vector size modification</b>
- <b> Sorting speed adjustment</b>
- <b> Single step simulaiton</b>
- <b> Soritng complexity counter</b>

## Technologies
- <b>C++ 17</b>
- <b>QT 6.2 QML</b>
- <b>CMake</b>
- <b>Google Test</b>
- <b>Google Mock</b>

## How to Build
#### Option 1. Production Docker Image (App + Libraries)
```
$ xhost local:root
$ docker run -it -e DISPLAY=$DISPLAY --net=host piotrchrzanowski/sortingvisualizator:production
```

#### Option 2. Develop Docker Image (Developer environment (Qt 6.2 etc.))
```
$ xhost local:root
$ docker run -it -e DISPLAY=$DISPLAY --net=host --volume ${PATH_TO_APP}:/app piotrchrzanowski/sortingvisualizator:develop
```

#### Option 3. Sources
Dependencies:
- <b>QT 6.2</b>
- <b>CMake</b>
```
$ git clone https://gitlab.com/piotr_chrzanowski/sorting-visualizator.git
$ git submodule init --recursive
$ git submodule update --recursive
$ mkdir build
$ cd build
$ cmake ..
$ make all
$ ./sorting-visualizator
```

## Usage Example
<table align="center"><tr>
<td align="center" width="1800">
<img src="doc/usage_example.mp4"  align="center" width="1800" height="900">
</td>
</tr>
<tr>
<td align="center">
[Usage Example](doc/usage_example.mp4).
</td>
</tr>
</table>

## Licence
This project is licensed under the terms of the **MIT** license.
