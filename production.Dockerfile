FROM piotrchrzanowski/sortingvisualizator:develop AS builder
COPY ./ /app/
WORKDIR /app/
RUN cqtdeployer -bin "./build/sorting-visualizator" -qmlDir "./gui/" -qmake "/opt/Qt6.2.2/bin/qmake"

FROM ubuntu:20.04
SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y 
RUN apt-get install -y build-essential
RUN apt-get update && apt-get install -y libfontconfig1-dev \
 libfreetype6-dev libx11-dev libx11-xcb-dev \
 libxext-dev \
 libxfixes-dev \
 libxi-dev \
 libxrender-dev \
 libxcb1-dev \
 libxcb-glx0-dev \
 libxcb-keysyms1-dev \
 libxcb-image0-dev \
 libxcb-shm0-dev \
 libxcb-icccm4-dev \
 libxcb-sync-dev \
 libxcb-xfixes0-dev \
 libxcb-shape0-dev \
 libxcb-randr0-dev \
 libxcb-render-util0-dev \
 libxcb-util-dev \
 libxcb-xinerama0-dev \
 libxcb-xkb-dev \
 libxkbcommon-dev \
 libxkbcommon-x11-dev \
 x11vnc xvfb \
 libopengl0

RUN apt-get update && apt-get install -y python-dev python3-dev
COPY --from=builder /app/DistributionKit /app/
WORKDIR /app/
CMD ./sorting-visualizator.sh
