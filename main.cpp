#include "BarChartController.h"
#include "SimulationController.h"
#include "SortingController.h"
#include "SortingActionsConverter.h"
#include "SortingActionsPublisher.h"
#include "StatisticsPanelController.h"
#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    std::shared_ptr<SortingActionsPublisher> actionsPublisher = std::make_shared<SortingActionsPublisher>();
    std::shared_ptr<SortingController> sortingController = std::make_shared<SortingController>(actionsPublisher);
    std::shared_ptr<BarChartController> chartController = std::make_shared<BarChartController>();
    std::shared_ptr<StatisticsPanelController> statisticsPanel = std::make_shared<StatisticsPanelController>();

    
    std::shared_ptr<SortingActionsConverter> sortingActionConverter = std::make_shared<SortingActionsConverter>(actionsPublisher, chartController, statisticsPanel);
    std::shared_ptr<SimulationController> simulationController = std::make_shared<SimulationController>(sortingController, sortingActionConverter);

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("chartController", chartController.get());
    engine.rootContext()->setContextProperty("statisticsPanelController", statisticsPanel.get());
    engine.rootContext()->setContextProperty("simulationController", simulationController.get());

    const QUrl url(QStringLiteral("qrc:/gui/main.qml"));
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
